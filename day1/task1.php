<?php

include_once '../bootstrap.php';

$inputRows = readFileByLines(__DIR__ . '/task-input.txt');
$increasesCount = 0;

$previous = 0;
$totalRows = count($inputRows);

for ($i = 0; $i < $totalRows; $i++) {
    if ($inputRows[$i] > $previous) {
        $increasesCount++;
    }

    $previous = $inputRows[$i];
}

print "Total increases " . $increasesCount . "\n";
print "\n";



