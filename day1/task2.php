<?php

include_once '../bootstrap.php';

$inputRows = readFileByLines(__DIR__ . '/task-input.txt');
//$inputRows = [
//    199,
//    200,
//    208,
//    210,
//    200,
//    207,
//    240,
//    269,
//    260,
//    263,
//];

$previous = 0;
$increasesCount = 0;
$totalRows = count($inputRows);
for ($i = 2; $i < $totalRows; $i++) {
    $currentSum = ((int) $inputRows[$i-2 ] +(int)$inputRows[$i -1] + (int) $inputRows[$i]);
    if ($currentSum > $previous && $previous !== 0) {
        $increasesCount++;
    }

    $previous = $currentSum;
}

print "Total increases " . $increasesCount . "\n";
print "\n";



