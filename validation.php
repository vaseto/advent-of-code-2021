<?php

/**
 * Validate a year within a time range
 *
 * @param string $candidate
 * @param array $rules
 * @return bool
 */
function validateYear(string $candidate, array $rules = []): bool
{
    if (preg_match('/^(?P<year>\d{4})$/i', $candidate, $match)) {
        if ($match['year'] >= $rules['min'] && $match['year'] <= $rules['max']) {
            return true;
        }
    }
    return false;
}

/**
 * Validate a Passport ID - exactly 9 numbers, including leading 0
 *
 * @param string $candidate
 * @return bool
 */
function validatePid(string $candidate): bool
{
    if (preg_match('/^\d{9}$/i', $candidate, $match)) {
        return true;
    }
    return false;
}

/**
 * Validate a hex colour
 *
 * @param string $candidate
 * @return bool
 */
function validateHexColor(string $candidate): bool
{
    if (preg_match('/^#([0-9a-f]{2}){3}$/i', $candidate, $match)) {
        return true;
    }
    return false;
}

/**
 * Validate eye colours
 *
 * @param string $candidate
 * @param $rules
 * @return bool
 */
function validateEyes(string $candidate, $rules): bool
{
    if (in_array($candidate, $rules)) {
        return true;
    }
    return false;
}

/**
 * Validate height in inches and centimeters
 *
 * @param string $candidate
 * @param array $rules
 * @return bool
 */
function validateHeight(string $candidate, array $rules): bool
{
    if (preg_match('/^(?P<value>\d{2,3})(?P<measurement>in|cm)$/i', $candidate, $match)) {
        if ($rules[$match['measurement']]['min'] <= (int) $match['value']
            && $rules[$match['measurement']]['max'] >= (int)  $match['value']
        ) {
            return true;
        }
    };

    return false;
}

/**
 * @param string $type
 * @param string $candidate
 * @param array $rules
 * @return bool
 */
function validate(string $type, string $candidate, array $rules = []): bool
{
    switch ($type) {
        case 'year':
            return validateYear($candidate, $rules);
        case 'colour':
            return validateHexColor($candidate);
        case 'pid':
            return validatePid($candidate);
        case 'height':
            return validateHeight($candidate, $rules);
        case 'eyes':
            return validateEyes($candidate, $rules);

    };
    return false;
}