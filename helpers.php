<?php

/**
 * This file contains helper functions
 */


/**
 * Read a file line by line and convert it to array
 *
 * @param string $inputTarget
 * @return array
 */
function readFileByLines(string $inputTarget): array
{
    $fileHandler = fopen($inputTarget, "r") or die("Unable to open file!");

    $fileContents = [];
    while ($row = fgets($fileHandler)) {
        $fileContents[] = $row;
    }
    fclose($fileHandler);
    return $fileContents;
}

include __DIR__ . DIRECTORY_SEPARATOR . 'validation.php';